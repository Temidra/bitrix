<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "кухня");
$APPLICATION->SetPageProperty("title", "Кухня эконом");
$APPLICATION->SetTitle("Кухонный гарнитур");

$kitchenList = [
	[
		'active' => true,
		'sale' => true,
		'name' => 'Береза-2000',
		'price' => 50000,
		'address' => 'Площадь Ленина 1',
		'img' => 'https://mebelland34.ru/image/cache/catalog/kuxni/dial/modulnie/modernprovansgorchica-700x800-1600x1400%281%29-1600x1400.jpg'
	],
	[
		'active' => true,
		'name' => 'Свежесть-8',
		'price' => 40000,
		'address' => 'Галушина 9',
		'img' => ''
	],
	[
		'active' => false,
		'name' => 'Монако',
		'price' => 54580,
		'address' => 'Свободы 3',
		'img' => 'https://volgodonsk.lubidom.ru/upload/iblock/552/5528a086f382a324d83c67a15bd5aabc.jpg'
	]
];

?>

<h1>Кухня</h1>

<p>Сегодняшняя дата: <?=$currentDate;?></p>

<?php
	foreach ($kitchenList as $kitchen) {

		if ($kitchen['active']) {

			$classList = 'kitchen';

			if ($kitchen['sale']) {
				$classList .= ' ' . 'kitchen_sale';
			}

			echo '
			<div class="' . $classList . '">
				<h3>' . $kitchen['name'] . '</h3>
				<p>Установлена по адресу:' . $kitchen['address'] . '<p>
				<p>Цена ' . $kitchen['price'] . ' ₽</p>';

				if (!empty($kitchen['img'])) {
					echo '<img src="' . $kitchen['img'] .'" alt="' . $kitchen['name'] .'">';
				}


			echo '</div>';
		}
	}
?>


<style>
.kitchen {
	padding: 30px;
	margin-bottom: 30px;
	border-radius: 10px;
	background-color: #fafafa;
}

.kitchen_sale {
	background-color: yellow;
}

.kitchen h3 {
	margin-top: 0;
}

.kitchen img {
	width: 100%;
	height: auto;
}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>