<?php

$officeList = [
    'Архангельск, улица Ленина, дом 1',
    'Северодвинск, улица Гагарина, дом 2',
    'Вельск, улица Бумажников, дом 4'
];

$officeListHtml = '';

foreach ($officeList as $office) {
    $officeListHtml .= "<p>{$office}</p>";
}
?>

<div class="address">
    <p class="address__heading">Наши офисы:</p>
    <?=$officeListHtml;?>

</div>

<style>
    .address {
        background-color: #fafafa;
        border-radius: 10px;

        font-size: 16px;
        font-weight: bold;
    }

    .address__heading {
        text-decoration: underline;
    }
</style>